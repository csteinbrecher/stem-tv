# stem-tv
Angular 4 app to display information about the WSU Asiku STEM Center events on a TV

## Build Instructions
To build for deployment to the PCs to be attached to the TVs:
```
ng build --prod --base-href ./
```

## Google Calendar Integration
Information about the Google Calendars to pull events from should go in the file `/assets/calendar-configuration.json`
which has the format:

```json
{
  "googleCalendarAPIKey": "your API key here",
  "googleCalendarIds": [
    "your first calendar id here",
    "your second calendar id here"
  ]
}
```

There can be any number of calendar ids in the array.

## Carousel Images
Information about the images for the carousel should go in the file `/assets/carousel-configuration.json`
which has the format:

```json
{
  "carouselImages": [
    "./assets/carousel-images/a.jpg",
    "./assets/carousel-images/b.jpg"
  ]
}
```

There can be any number of image file/paths in the array.
