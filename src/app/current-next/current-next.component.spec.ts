import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentNextComponent } from './current-next.component';

describe('CurrentNextComponent', () => {
  let component: CurrentNextComponent;
  let fixture: ComponentFixture<CurrentNextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentNextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentNextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
