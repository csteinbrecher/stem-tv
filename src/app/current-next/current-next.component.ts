import { Component, OnInit, OnDestroy } from '@angular/core';
import { CalendarEventsService } from '../calendar-events.service';

@Component({
  selector: 'app-current-next',
  templateUrl: './current-next.component.html',
  styleUrls: ['./current-next.component.scss']
})
export class CurrentNextComponent implements OnInit, OnDestroy {

  constructor(public calendarEventsService: CalendarEventsService) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.calendarEventsService.endTimers();
  }
}
