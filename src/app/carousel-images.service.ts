import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SimpleTimer} from 'ng2-simple-timer';

@Injectable()
export class CarouselImagesService {

  public carouselImages: Array<string> = [];
  timer1minId: string;

  constructor(private http: HttpClient, private timer: SimpleTimer) {
    // get images on startup
    this.http.get('./assets/carousel-configuration.json').subscribe( (response) => {
      console.log(response);
      this.carouselImages = response['carouselImages'];
      console.log(this.carouselImages);
    });

    // get images every 15 minutes
    this.timer1minId = this.timer.subscribe(
      '1min',
      () => {
        let minutes: string = new Date(Date.now()).toTimeString().substr(3,2);
        if (minutes === '00' || minutes === '15' || minutes === '30' || minutes === '45') {
          this.http.get('./assets/carousel-configuration.json').subscribe( (response) => {
            this.carouselImages = response['carouselImages'];
          });
        }
      });
  }

  endTimers() {
    this.timer.delTimer('1min');
  }
}
