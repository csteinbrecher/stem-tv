export class CalendarEvent {
  public summary: string;
  public location: string;
  public start: { dateTime: string };
  public end: { dateTime: string };
  public description: string;
}
