import { Component, OnInit } from '@angular/core';
import { CarouselImagesService } from '../carousel-images.service';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {

  constructor(public carouselImagesService: CarouselImagesService) {

  }

  ngOnInit() {
  }


}
